'use strict';


const { WorkloadModuleBase } = require('@hyperledger/caliper-core');


class MyWorkload extends WorkloadModuleBase {
    constructor() {
        super();
        this.txIndex = 0;
    }


    async initializeWorkloadModule(workerIndex, totalWorkers, roundIndex, roundArguments, sutAdapter, sutContext) {
        await super.initializeWorkloadModule(workerIndex, totalWorkers, roundIndex, roundArguments, sutAdapter, sutContext);


    }


    async submitTransaction() {
        const assetID = `${this.workerIndex}_${this.txIndex}`;
        this.txIndex++;
        console.log(`Worker ${this.workerIndex}: Creating asset ${assetID}`);
        const myArgs = {
            contractId: this.roundArguments.contractId,
            contractFunction: 'CreateAsset',
            invokerIdentity: 'User1',
            contractArguments: [assetID, 'blue', '20', 'penguin', '500'],
            readOnly: false
        };


        await this.sutAdapter.sendRequests(myArgs);
    }


    async cleanupWorkloadModule() {
        for (let i = 0; i < this.txIndex; i++) {
            const assetID = `${this.workerIndex}_${i}`;
            console.log(`Worker ${this.workerIndex}: Deleting asset ${assetID}`);
            const request = {
                contractId: this.roundArguments.contractId,
                contractFunction: 'DeleteAsset',
                invokerIdentity: 'User1',
                contractArguments: [assetID],
                readOnly: false
            };


            await this.sutAdapter.sendRequests(request);
        }
    }
}


function createWorkloadModule() {
    return new MyWorkload();
}


module.exports.createWorkloadModule = createWorkloadModule;
